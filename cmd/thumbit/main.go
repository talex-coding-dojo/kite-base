package main

import (
	"gitlab.com/talex-coding-dojo/kite-base/handler"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/nats-io/nats.go"
	log "github.com/sirupsen/logrus"

	"gitlab.com/talex-coding-dojo/kite-base/middleware"
	my_nats "gitlab.com/talex-coding-dojo/kite-base/nats"
	"gitlab.com/talex-coding-dojo/kite-base/repository"
)

func init() {
	// Init connection to NATS streaming server
	my_nats.InitNATS()
}

func main() {
	// Init logger
	middleware.InitLogrus()

	// Start interrupt handler
	trapInterrupts()

	// Start timer
	start := time.Now()

	// Init database connection
	repository.InitDB()

	// Register handler
	subject := string(my_nats.PictureCreatedEvent)

	// Simple async ephemeral consumer
	_, err := my_nats.Context.Subscribe(subject, func(m *nats.Msg) {
		log.Debugf("Received a JetStream message: %s\n", string(m.Data))
		handler.PictureCreatedHandler(m)
	})
	if err != nil {
		log.WithError(err).Fatal("could not complete NATS consumer startup")
	}

	log.WithFields(log.Fields{"Subject": subject}).Warn("registered NATS event handler")
	// Stop timer
	duration := middleware.GetDurationInMillis(start)
	log.WithField("Latency", duration).Info("NATS consumer startup completed")

	// Begin infinite consumer loop
	for {
		select {}
	}
}

func trapInterrupts() {
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)

	go func() {
		sig := <-sigs

		my_nats.CloseNATS()
		repository.CloseDB()

		log.WithField("Signal", sig).Warn("exited worker")
		os.Exit(1)
	}()
}
