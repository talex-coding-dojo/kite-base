FROM golang:1.20-alpine

# Set App ENV variables
ENV POSTGRES_HOSTNAME "postgresql.postgresql"
ENV POSTGRES_PORT "5432"
ENV POSTGRES_USERNAME "kite"
ENV POSTGRES_PASSWORD "secret"
ENV POSTGRES_DATABASE "kite_base"

ENV LOGGING_LEVEL "info"
ENV LOGGING_FORMAT "json"

ENV APP_IP_ADDRESS "0.0.0.0"
ENV APP_PORT "8080"
ENV APP_PRODUCTION_MODE "true"

# Set Go ENV variables
#ENV GO111MODULE=on
#ENV CGO_ENABLED=0
#ENV GOOS=linux
#ENV GOARCH=amd64

# Install packages
RUN apk update
RUN apk add make bash
RUN apk add ca-certificates git

# Move to working directory
RUN mkdir /go/src/kite-base
WORKDIR /go/src/kite-base

# Install helper
RUN git clone https://github.com/vishnubob/wait-for-it.git

# Copy source code into container
COPY . .

# Download dependencies
RUN go get -d -v ./...

# Build migrations
RUN go build -v -o kb-migrate migrate/*.go

# Build NATS consumer
RUN go build -v -o kb-thumbit cmd/thumbit/*.go

# Build the Go application
RUN go build -v -o kb-server *.go

# Move to resulting binary folder
RUN mkdir /dist
WORKDIR /dist

# Copy binary from build to main folder
RUN cp -v /go/src/kite-base/kb-server /go/src/kite-base/kb-migrate /go/src/kite-base/kb-thumbit .
RUN cp -v /go/src/kite-base/wait-for-it/wait-for-it.sh .

RUN touch .env

# Expose server port
EXPOSE $APP_PORT

# Run the server
CMD ["/dist/kb-server"]
