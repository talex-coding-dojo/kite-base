package mapper

import (
	"fmt"

	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
	"google.golang.org/protobuf/types/known/timestamppb"

	dto "gitlab.com/talex-coding-dojo/kite-base/controller/model"
	"gitlab.com/talex-coding-dojo/kite-base/pb"
	"gitlab.com/talex-coding-dojo/kite-base/pb/events"
	"gitlab.com/talex-coding-dojo/kite-base/pb/types"
)

type ProtoMapper struct{}

func GetProtoMapper() *ProtoMapper {
	return &ProtoMapper{}
}

func (m *ProtoMapper) FromPictureCreated(id string, pbPicture *types.MediaFile) (dtoPicture *dto.Picture, err error) {
	var pictureUUID uuid.UUID

	dtoPicture = &dto.Picture{
		BasePicture: dto.BasePicture{
			Description: fmt.Sprintf("%s [%s]", pbPicture.Description, "Thumbnail"),
			Height:      pbPicture.Height,
			Width:       pbPicture.Width,
			Data:        pbPicture.Data,
		},
	}

	pictureUUID, err = uuid.FromString(id)
	if err != nil {
		log.WithError(err).Error("could not parse UUID: %s", id)
		return nil, err
	}

	dtoPicture.UUID = pictureUUID

	return dtoPicture, nil
}

func (m *ProtoMapper) ToPictureCreated(dtoPicture *dto.Picture) (pbEnvelope *pb.Envelope) {
	pbEnvelope = m.buildEnvelopeForEvent()

	pbEnvelope.Payload.(*pb.Envelope_Event).Event.Event = &pb.Event_PictureCreated{
		PictureCreated: &events.PictureCreated{
			Id: &types.UUID{
				Value: dtoPicture.UUID.String(),
			},
			Picture: &types.MediaFile{
				Data:        dtoPicture.Data,
				Description: dtoPicture.Description,
				MimeType:    dtoPicture.MimeType(),
				Height:      dtoPicture.Height,
				Width:       dtoPicture.Width,
			},
		},
	}

	return pbEnvelope
}

func (m *ProtoMapper) buildEnvelopeForEvent() *pb.Envelope {
	return &pb.Envelope{
		SentAt:        timestamppb.Now(),
		SenderName:    "kite-base",
		CorrelationId: uuid.NewV4().String(),
		Payload: &pb.Envelope_Event{
			Event: &pb.Event{},
		},
	}
}
