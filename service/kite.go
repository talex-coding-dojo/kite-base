package service

import (
	uuid "github.com/satori/go.uuid"

	dto "gitlab.com/talex-coding-dojo/kite-base/controller/model"
	"gitlab.com/talex-coding-dojo/kite-base/mapper"
	"gitlab.com/talex-coding-dojo/kite-base/repository"
	db "gitlab.com/talex-coding-dojo/kite-base/repository/model"
)

type KiteService struct {
	bagRepository       *repository.BagRepository
	pictureRepository   *repository.PictureRepository
	sparePartRepository *repository.SparePartRepository
	kiteRepository      *repository.KiteRepository
	mapper              *mapper.KiteMapper
}

func GetKiteServiceInstance() *KiteService {
	return &KiteService{
		bagRepository:       repository.GetBagRepository(),
		pictureRepository:   repository.GetPictureRepository(),
		sparePartRepository: repository.GetSparePartRepository(),
		kiteRepository:      repository.GetKiteRepository(),
		mapper:              mapper.GetKiteMapper(),
	}
}

func (s *KiteService) Create(data *dto.KiteCreation) (kite *dto.Kite, err error) {
	picture, err := s.pictureRepository.FetchByID(data.PictureID)
	if err != nil {
		return nil, err
	}

	bag, err := s.bagRepository.FetchByID(data.BagID)
	if err != nil {
		return nil, err
	}

	dbKite := &db.Kite{
		ID:                   uuid.NewV4(),
		PictureID:            picture.ID,
		Picture:              picture,
		BagID:                bag.ID,
		Bag:                  bag,
		DisplayName:          data.DisplayName,
		ManufacturerName:     data.ManufacturerName,
		ModelName:            data.ModelName,
		NumLines:             data.NumLines,
		Stackable:            data.Stackable,
		Wind:                 db.WindLevel(data.Wind),
		Type:                 db.ConstructionType(data.Type),
		AdditionalAttributes: data.AdditionalAttributes,
	}

	for _, sparePart := range data.SpareParts {
		dbSparePart := &db.SparePart{
			ID:          uuid.NewV4(),
			Name:        sparePart.Name,
			Description: sparePart.Description,
			Quantity:    sparePart.Quantity,
		}

		err := s.sparePartRepository.Store(dbSparePart)
		if err != nil {
			return nil, err
		}

		dbKite.SpareParts = append(dbKite.SpareParts, dbSparePart)
	}

	err = s.kiteRepository.Store(dbKite)
	if err == nil {
		kite, err = s.mapper.ToDTO(dbKite)
	}

	return kite, err
}

func (s *KiteService) FindOneByUUID(uid uuid.UUID) (kite *dto.Kite, err error) {
	dbKite, err := s.kiteRepository.FetchByID(uid)
	if err == nil {
		kite, err = s.mapper.ToDTO(dbKite)
	}

	return kite, err
}
