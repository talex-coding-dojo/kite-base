package nats

import (
	"github.com/nats-io/nats.go"
	"github.com/penglongli/gin-metrics/ginmetrics"
	log "github.com/sirupsen/logrus"
	"google.golang.org/protobuf/encoding/protojson"
	"google.golang.org/protobuf/proto"

	"gitlab.com/talex-coding-dojo/kite-base/config"
	"gitlab.com/talex-coding-dojo/kite-base/pb"
)

var Conn *nats.Conn

var Context nats.JetStreamContext

const (
	StreamName     = "MEDIA"
	StreamSubjects = "MEDIA.*"
)

func InitNATS() {
	// Connect to NATS
	url := config.App.NATS.ServerURL
	clientID := config.App.NATS.ClientID

	Conn, err := nats.Connect(url, nats.Name(clientID))
	if err != nil {
		log.WithError(err).Fatal("could not connect NATS")
	}

	// Create JetStream Context
	Context, err = Conn.JetStream(nats.PublishAsyncMaxPending(256))
	if err != nil {
		log.WithError(err).Fatal("could not connect JetStream")
	}

	// Create stream
	stream, err := Context.StreamInfo(StreamName)

	// stream not found, create it
	if stream == nil {
		log.Printf("Creating stream: %s\n", StreamName)

		_, err = Context.AddStream(&nats.StreamConfig{
			Name:     StreamName,
			Subjects: []string{StreamSubjects},
		})
		if err != nil {
			log.WithError(err).Fatal("could not create stream")
		}
	}

	log.WithFields(log.Fields{"url": url, "client_id": clientID}).Warn("successfully connected NATS")
}

func CloseNATS() {
	if Context == nil {
		return
	}

	Conn.Close()
	Conn = nil
}

func Publish(messageType string, envelope *pb.Envelope) {
	binary, err := proto.Marshal(envelope)
	if err != nil {
		log.WithError(err).Error("could not serialize event")
		return
	}

	pubAck, err := Context.Publish(messageType, binary)
	if err != nil {
		log.WithError(err).Error("could not publish event")
		_ = ginmetrics.GetMonitor().GetMetric("emitted_events_failure_count").Inc([]string{messageType})
		return
	}

	_ = ginmetrics.GetMonitor().GetMetric("emitted_events_success_count").Inc([]string{messageType})

	if log.GetLevel() == log.DebugLevel {
		json, err := protojson.Marshal(envelope)
		if err != nil {
			log.WithError(err).Debug("could not serialize JSON for log entry")
			return
		}

		log.WithField("event_payload", string(json)).Debugf("published %q event", messageType)
		return
	}

	pictureUUID := envelope.Payload.(*pb.Envelope_Event).Event.GetPictureCreated().Id.Value
	log.WithFields(log.Fields{
		"CorrelationId": envelope.CorrelationId,
		"PictureUUID":   pictureUUID,
		"Stream":        pubAck.Stream,
	}).Infof("published %q event", messageType)
}
