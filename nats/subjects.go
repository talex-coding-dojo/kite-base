package nats

type MessageType string

const (
	PictureCreatedEvent MessageType = "MEDIA.PictureCreated"
)
