# Coding Dojo - Kite Base // RESTful API Example in Golang

![The Human Kite](https://media.giphy.com/media/l0HlUndS1mL7bG86I/giphy.gif)

This repository is part of my Golang Meetup and Workshop Series.

See [Workshop ReadMe](LINKS.md) for topics, learnings and links from my Meetup __Let's Go RESTful__.

## Summary

We build a REST API in Golang for a Database that stores kites. Follow the [Table of Contents](LINKS.md), [branches](-/branches/active) and [commits](-/commits/master) for a step by step development of this sample project.

## Getting Started

For development with [Go](https://golang.org/) you need this toolchain in your computer:

- Download and install [Goland](https://www.jetbrains.com/go/) IDE from Jetbrains. Test version is 30 days for free.
- Download a [Go binary release](https://golang.org/dl/) for your system. On MacOS computers you can also use [Homebrew](https://formulae.brew.sh/formula/go) for installation.
  This will install the `go` binary which acts as compiler, package manager, tester and starter.
- Install [Postgres](https://www.postgresql.org/) database or use [PostgreSQL as a Service](https://www.elephantsql.com/) where you can get one DB for free.
- Some additional tools like `docker` and `docker-compose`.

To create and init a new Golang project you just need to:

- Create a new project from Goland IDE or just make a new empty directory in `~/go/src/` on your system.
- Add `.gitignore` file and a `build` directory inside.
- Init Go modules with `go mod init gitlab.com/talex-coding-dojo/kite-base` and use your public Git repo path as module name.
- Create a `main.go` file in the projects root dir. It belongs to `package main` and has a `main()` function.

Visit [a tour of Go](https://tour.golang.org/welcome/1) to get a first impression of the language.

See [Go cheatsheet](https://devhints.io/go) for syntax overview or [Go Code Samples](https://github.com/a8m/golang-cheat-sheet) for a deeper syntax reminder with code examples..

## Development

- migrations: `go build -o build/migrate migrate/*.go && build/migrate`
- start server: `go build -o build/server main.go && ./build/server`
- test: `go test -c repository/*.go -o build/repository.test && ./build/repository.test -test.v`

## Docker

- build image: `docker build -t kite-base:latest .`
- start everything using docker-compose: `docker-compose up --build`

## REST API

You can use the [Postman Collection](postman.json) to setup a testing environment in Postman for this project.

### Bags

#### List all Kite Bags

_URL_: `GET http://localhost:9090/bags?name=primary`

_Query Parameter_: add `name` to search for bag names

_Response Body_:

```json
[
    {
        "name": "My Primary Kite Bag",
        "uuid": "0ee5c0e0-2e62-4e52-8fcf-a785eea44171",
        "num_kites": 1
    },
    {
        "name": "My Other Kite Bag",
        "uuid": "033ebfee-7612-41b0-8998-46e07f7e5fa6",
        "num_kites": 0
    }
]
```

#### Create new kite bag

_URL_: `POST http://localhost:9090/bags`

_Request Body_:

```json
{
    "name": "My Other Own Kite Bag"
}
```

_Response Body_:

```json
{
    "name": "My Other Own Kite Bag",
    "uuid": "033ebfee-7612-41b0-8998-46e07f7e5fa6",
    "num_kites": 0
}
```

#### Update a Kite Bag

_URL_: `PUT http://localhost:9090/bags/:id`

_Path Parameter_: provide UUID `id` to declare Kite Bag to be updated

_Request Body_:

```json
{
    "name": "My Other Own Kite Bag Renamed"
}
```

_Response Body_:

```json
{
    "name": "My Other Own Kite Bag Renamed",
    "uuid": "033ebfee-7612-41b0-8998-46e07f7e5fa6",
    "num_kites": 0
}
```

#### Show a Kite Bag

_URL_: `GET http://localhost:9090/bags/:id`

_Path Parameter_: provide UUID `id` to declare Kite Bag to be shown

_Response Body_:

```json
{
    "name": "My Other Own Kite Bag Renamed",
    "uuid": "033ebfee-7612-41b0-8998-46e07f7e5fa6",
    "num_kites": 0
}
```

#### Destroy a Kite Bag

_URL_: `DELETE http://localhost:9090/bags/:id`

_Path Parameter_: provide UUID `id` to declare Kite Bag to be deleted

_Response Status_: `204 No Content` or `404 Not Found`

## Event Bus

### NATS Tools

Install some NATS tools:

```bash
go get github.com/nats-io/nats-top
go get github.com/shogsbro/natscat
```

If your NATS port `8222` is exposed you can connect the event bus with NATS tools:

```bash
nats-top

natscat -l -s test -b &
natscat -s test -m "Foo Bar"
``` 