# Let's Go(lang) RESTful

Check out [Go cheatsheet](https://devhints.io/go) from `devhints.io` for a quick reference of Golang syntax.

## Database Model

![Goland exported DB Diagramm](db-model.png)

## Learnings by Chapter

### Setup Database Model

Branch: [setup database model](../../commit/9c65df9aed6cce40b25989a36985b64154c1e4e2)

- define a [go.mod](https://github.com/golang/go/wiki/Modules#gomod) for Go modules tree in root dir
- how to install Go modules, for example [go.uuid](github.com/satori/go.uuid)
- organize Golang code using [package](https://golang.org/doc/code.html#Organization)
- build DB models using [struct](https://gobyexample.com/structs)
- add [value type](https://gobyexample.com/values) fields to DB models
- use [pointers](https://gobyexample.com/pointers) to model relations
- use [constants](https://gobyexample.com/constants) to build `string` enums
- add "toString" function to a struct with [func](https://gobyexample.com/functions)
- iterate [slices](https://gobyexample.com/slices) with [range](https://gobyexample.com/range)
- use `interface{}` for unspecified types
- use [defer](https://gobyexample.com/defer) to trigger function calls later for e.g. cleanup
- use [maps](https://gobyexample.com/maps) and `append` to build hashes
- handle [errors](https://gobyexample.com/errors) (there are no exceptions in Golang)
- use `_` to ignore results from `func` calls
- use Golang [std-lib](https://golang.org/pkg/#stdlib):
    - using [fmt](https://golang.org/pkg/fmt/) to format strings C-style
    - using [time](https://golang.org/pkg/time/) to handle timestamps
- add database persistence with [go-pg](https://github.com/go-pg/pg/) ORM for Golang and [Postgres](https://www.postgresql.org)
    - add Golang field tags for [go-pg](https://pg.uptrace.dev/models/) database persistence
    - how to use relations ([belongs-to](https://pg.uptrace.dev/queries/#belongs-to), [has-many](https://pg.uptrace.dev/queries/#has-many), [has-many-to-many](https://pg.uptrace.dev/queries/#has-many-to-many))
    - use [hooks](https://pg.uptrace.dev/hooks/) from `go-pg` lib
    - push Golang `map` hashes to PG using [hstore](https://www.postgresql.org/docs/12/hstore.html) columns
    - how to connect Postgres database
    - add database [migrations](https://pg.uptrace.dev)
    - add records with [insert](https://pg.uptrace.dev/queries/#insert)
    - fetch records and relations with [select](https://pg.uptrace.dev/queries/#select) and [relation](https://pg.uptrace.dev/queries/#column-names)
- use [go-spew](https://github.com/davecgh/go-spew) to pretty print Go data structures

### Config for Application

Branch: [config for application](../../commit/1ee52d58ba95dc557d60010f7bd6b4c51bb64d75)

- add ENV support with [genv](github.com/sakirsensoy/genv)
    - define `Default()` values, supports type conversions (aka "typecasting")
    - add local ENV config to autoloaded `.env` file
- use `_` to force `import` of unused packages
- using [strconv](https://golang.org/pkg/strconv/) from Golang std-lib for basic type conversions
- access [exported](https://yourbasic.org/golang/public-private/) identifiers from imported packages

### Add an Endpoint for Kite Bags

Branch: [ctrl, svc and repo for kite bags](../../commit/d29c616d6462fc585f520c27e5105f950e520f18)

- using [net/http](https://golang.org/pkg/net/http/) from Golang std-lib for HTTP requests and responses
- add Go `main` program for the REST API server
- add the [Gin](https://github.com/gin-gonic/gin) HTTP router from [Gin Web Framework](https://gin-gonic.com/)
    - how to use [path parameters](https://github.com/gin-gonic/gin#parameters-in-path) and [query string](https://github.com/gin-gonic/gin#querystring-parameters)
    - [model binding](https://github.com/gin-gonic/gin#model-binding-and-validation) and validation, works also for [query string](https://github.com/gin-gonic/gin#only-bind-query-string)
    - implement [custom validators](https://github.com/gin-gonic/gin#custom-validators) and use them in Go tags
    - [group routes](https://github.com/gin-gonic/gin#grouping-routes) to structure endpoints
    - how to implement [middleware](https://github.com/gin-gonic/gin#using-middleware) in Gin and log service latency
    - graceful [shutdown](https://github.com/gin-gonic/gin#graceful-shutdown-or-restart) and restarts
    - how to return JSON data from a controller action
- implement `/bags` REST API endpoint:
    - controller and actions
    - init router, group routes by endpoints
    - connect HTTP paths and methods to controller actions
    - use [panic](https://gobyexample.com/panic) if something goes wrong
- how to build a "rich" `NotFoundError` error in Go using `struct`
- implement repository with `Store()`, `Update()`, `Delete()`, `FetchById()` and `FetchAll()`
- how to use `go-pg` to load and save data, including model associations
- implement mapper to convert DTO to DB models and vice versa
- implement service layer to negotiate between controller and persistence layer

#### Extra

- add [CORS](https://github.com/gin-contrib/cors) middleware to Gin

### Add JSON Logging

Branch: [JSON logging](../../commit/cb21e2ab9c1f26f4b4fdc118a5ac6146855126c4)

- add JSON  support with [genv](github.com/sakirsensoy/genv)
- log structure data using:
    - `WithFields()`
    - `WithField()`
    - `WithError()`
- extend middleware for JSON logging
- extend App configuration for JSON logging

### Trap Interrupts

Branch: [trap interrupts](../../commit/90f1266cf9d771e51f67be16a9d97adcdb29ce8b)

- use [channels](https://gobyexample.com/channels) to connect [Goroutines](https://gobyexample.com/goroutines)
- handle [signals](https://gobyexample.com/signals) and [exit](exit) App with status

### Add an Endpoint for Pictures

Branch: [ctrl, svc and repo for kite pictures](../../commit/06f5a730d314aaae6612d0b7e74505dc50f95ccf)

- more examples for JSON bindings in DTO models
- additional example for controller
- use [nested struct](https://www.golangprograms.com/nested-struct-type-in-go-programming-language.html) types
- handle 404 errors in controller action if repository did not found a record
- use [switch](https://gobyexample.com/switch) statement to check error types.

### Add an Endpoint for Kites

Branch: [ctrl, svc and repo for kites](../../commit/542615ac0b9d0c3c1fa69a0bed82b5fe38511c0b)

- one more example for repository, service and controller layer
- use embedded `struct` types to build-up DTO models
- resolve and manage a bit more complex associations between models
- how to work with many-to-many relationships in `go-pg`
- implement [testing](https://gobyexample.com/testing) for a repository

### Add an Endpoint for Image Rendering

Branch: [add andpoint for image rendering](../../commit/bc8e5bcc12a1ead350b5f02c8787349d31bc6df4)

- decode base64 encoded image data in service layer
- add controller action that returns the requested kite picture

## API Docs

- [pg](https://pkg.go.dev/github.com/go-pg/pg?tab=doc) (`github.com/go-pg/pg`)
- [gin](https://pkg.go.dev/github.com/gin-gonic/gin?tab=doc) (`github.com/gin-gonic/gin`)
- [logrus](https://pkg.go.dev/github.com/sirupsen/logrus?tab=doc) (`github.com/sirupsen/logrus`)
- [assert](https://pkg.go.dev/github.com/stretchr/testify@v1.6.1/assert?tab=doc) (`github.com/stretchr/testify/assert`)

## Resources

- How to work with [models](https://pg.uptrace.dev/models/), [queries](https://pg.uptrace.dev/queries/) and [hooks](https://pg.uptrace.dev/hooks/) in `go-pg`
- [Go by Example](https://gobyexample.com) provides nice overview and examples for Golang
- [Comparison of popular web frameworks in Go](https://deepsource.io/blog/go-web-frameworks/)
