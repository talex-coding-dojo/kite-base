package repository

import (
	"errors"

	"github.com/go-pg/pg/v10"
	uuid "github.com/satori/go.uuid"

	"gitlab.com/talex-coding-dojo/kite-base/repository/model"
)

type PictureRepository struct{}

func GetPictureRepository() *PictureRepository {
	return &PictureRepository{}
}

func (r *PictureRepository) Store(picture *model.Picture) (err error) {
	_, err = db.Model(picture).Insert()
	return err
}

func (r *PictureRepository) Delete(picture *model.Picture) (err error) {
	_, err = db.Model(picture).Delete()
	return err
}

func (r *PictureRepository) FetchByID(id uuid.UUID) (picture *model.Picture, err error) {
	picture = &model.Picture{ID: id}
	query := db.Model(picture).WherePK()
	err = query.Select()

	// Handle Postgres 404 error
	if errors.As(err, &pg.ErrNoRows) {
		err = &NotFoundError{ID: id.String(), Err: err}
	}

	return picture, err
}

func (r *PictureRepository) FetchAll() (pictures []*model.Picture, err error) {
	pictures = []*model.Picture{}
	query := db.Model(&pictures)
	err = query.Select()

	return pictures, err
}

func (r *PictureRepository) Exists(id uuid.UUID) (result bool) {
	count, _ := db.Model((*model.Picture)(nil)).Where("uuid = ?", id).Count()
	return count == 1
}
