package model

import (
	"fmt"
	"time"

	"github.com/go-pg/pg/v10/orm"
	uuid "github.com/satori/go.uuid"
)

type WindLevel string

const (
	ULW WindLevel = "Ultra Light Wind"
	LW  WindLevel = "Light Wind"
	MW  WindLevel = "Medium Wind"
	SW  WindLevel = "Strong Wind"
	HW  WindLevel = "Heavy Wind"
)

type ConstructionType string

const (
	Delta    ConstructionType = "Delta Kite"
	Diamond  ConstructionType = "Diamond Kite"
	Foil     ConstructionType = "Parafoil Kite"
	Cellular ConstructionType = "Cellular Kite"
	Sled     ConstructionType = "Sled Kite"
	Rokkaku  ConstructionType = "Rokkaku Fighter Kite"
	Sport    ConstructionType = "Stunt Kite"
	Traction ConstructionType = "Traction Kite"
)

// see https://pg.uptrace.dev/models/

type Kite struct {
	// meta data
	tableName struct{}  `pg:"kites,alias:k"`
	ID        uuid.UUID `pg:"uuid,type:uuid"`

	// relations
	PictureID  uuid.UUID    `pg:"type:uuid,use_zero"`
	Picture    *Picture     `pg:"rel:has-one,fk:picture_id"`
	BagID      uuid.UUID    `pg:"type:uuid,use_zero"`
	Bag        *Bag         `pg:"rel:has-one,fk:bag_id"`
	SpareParts []*SparePart `pg:"many2many:kite_parts,join_fk:spare_part_uuid"`

	// fields
	DisplayName      string `pg:"type:varchar(100),unique,notnull"`
	ManufacturerName string `pg:"type:varchar(100),notnull"`
	ModelName        string `pg:"type:varchar(100),notnull"`
	NumLines         int    `pg:"default:1"`
	Stackable        bool   `pg:"default:false"`

	// enums
	Wind WindLevel
	Type ConstructionType

	// JSON hash map
	AdditionalAttributes map[string]string `pg:"attrs,hstore"`

	// timestamps
	CreatedAt  time.Time `pg:"default:now()"`
	ModifiedAt time.Time `pg:"default:now()"`
	DeletedAt  time.Time `pg:",soft_delete"`
}

func (k Kite) String() string {
	return fmt.Sprintf("Kite<UUID=%q DisplayName=%q CreatedAt=%s>", k.ID, k.DisplayName, k.CreatedAt)
}

func (k Kite) BeforeInsert(db orm.DB) error {
	if k.CreatedAt.IsZero() {
		k.CreatedAt = time.Now()
	}

	return nil
}
