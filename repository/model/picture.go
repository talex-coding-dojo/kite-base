package model

import (
	"fmt"

	uuid "github.com/satori/go.uuid"
)

// see https://pg.uptrace.dev/models/

type Picture struct {
	// meta data
	tableName struct{}  `pg:"media_files,alias:mf"`
	ID        uuid.UUID `pg:"uuid,type:uuid"`

	// fields
	Description string
	MimeType    string `pg:"type:varchar(100),notnull"`
	Height      int32
	Width       int32
	Data        string `pg:"base64_encoded_data,type:text,notnull"`
}

func (p Picture) String() string {
	return fmt.Sprintf("Picture<UUID=%q MimeType=%q, Height=%dpx Width=%dpx>", p.ID, p.MimeType, p.Height, p.Width)
}
