package model

import (
	uuid "github.com/satori/go.uuid"
)

// see https://pg.uptrace.dev/models/

type Bag struct {
	// meta data
	tableName struct{}  `pg:"alias:b"`
	ID        uuid.UUID `pg:"uuid,type:uuid"`

	// relations
	Kites []*Kite `pg:"rel:has-many,join_fk:bag_id"`

	// fields
	Name string `pg:"type:varchar(100),unique,notnull"`
}
