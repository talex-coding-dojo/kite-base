package repository

import (
	"fmt"
	"strconv"

	"github.com/go-pg/pg/v10"
	log "github.com/sirupsen/logrus"

	"gitlab.com/talex-coding-dojo/kite-base/config"
)

var db *pg.DB

func InitDB() {
	// Read Postgres config
	hostname := config.App.Database.Postgres.Hostname + ":" + strconv.Itoa(config.App.Database.Postgres.Port)
	username := config.App.Database.Postgres.Username
	password := config.App.Database.Postgres.Password
	database := config.App.Database.Postgres.Database

	// Connect Postgres database
	db = pg.Connect(&pg.Options{Addr: hostname, User: username, Password: password, Database: database})
	fields := log.Fields{"Hostname": hostname, "Username": username, "Database": database}

	// Send first SQL query to establish connection
	_, err := db.Exec("SELECT 1")
	if err != nil {
		if !config.App.ProductionMode {
			fields["Password"] = password
		}

		log.WithFields(fields).WithError(err).Panic("database init failed")
	}

	log.WithFields(fields).Warn("database connection initialized")
}

func CloseDB() {
	// Disconnect Postgres database
	db.Close()
	log.Warn("database connection closed")
}

type NotFoundError struct {
	ID  string
	Err error
}

func (e NotFoundError) Error() string {
	return fmt.Sprintf("could not find record with ID: %s", e.ID)
}
