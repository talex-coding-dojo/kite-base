package repository

import (
	"errors"
	"fmt"

	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
	uuid "github.com/satori/go.uuid"

	"gitlab.com/talex-coding-dojo/kite-base/repository/model"
)

func init() {
	// Register many to many model so ORM can better recognize m2m relation.
	// This should be done before dependant models are used.
	orm.RegisterTable((*model.KitePart)(nil))
}

type BagRepository struct{}

func GetBagRepository() *BagRepository {
	return &BagRepository{}
}

func (r *BagRepository) Store(bag *model.Bag) (err error) {
	_, err = db.Model(bag).Insert()
	return err
}

func (r *BagRepository) Update(bag *model.Bag) (err error) {
	_, err = db.Model(bag).Update()

	// Handle Postgres 404 error
	if errors.As(err, &pg.ErrNoRows) {
		err = &NotFoundError{ID: bag.ID.String(), Err: err}
	}

	return err
}

func (r *BagRepository) Delete(bag *model.Bag) (err error) {
	_, err = db.Model(bag).Delete()
	return err
}

func (r *BagRepository) FetchByID(id uuid.UUID) (bag *model.Bag, err error) {
	bag = &model.Bag{ID: id}
	query := db.Model(bag).Relation("Kites").WherePK()
	err = query.Select()

	// Handle Postgres 404 error
	if errors.As(err, &pg.ErrNoRows) {
		err = &NotFoundError{ID: id.String(), Err: err}
	}

	return bag, err
}

func (r *BagRepository) FetchAll() (bags []*model.Bag, err error) {
	bags = []*model.Bag{}
	query := db.Model(&bags).Relation("Kites")
	err = query.Select()

	return bags, err
}

func (r *BagRepository) FetchAllByNameLike(name string) (bags []*model.Bag, err error) {
	bags = []*model.Bag{}
	query := db.Model(&bags).Relation("Kites").Where("name LIKE ?", fmt.Sprintf("%s%%", name))
	err = query.Select()

	return bags, err
}

func (r *BagRepository) Exists(id uuid.UUID) (result bool) {
	count, _ := db.Model((*model.Bag)(nil)).Where("uuid = ?", id).Count()
	return count == 1
}
