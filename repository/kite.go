package repository

import (
	"errors"
	"fmt"

	"github.com/go-pg/pg/v10"
	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"

	"gitlab.com/talex-coding-dojo/kite-base/repository/model"
)

type KiteRepository struct {
	bagRepository       *BagRepository
	pictureRepository   *PictureRepository
	sparePartRepository *SparePartRepository
}

func GetKiteRepository() *KiteRepository {
	return &KiteRepository{
		bagRepository:       GetBagRepository(),
		pictureRepository:   GetPictureRepository(),
		sparePartRepository: GetSparePartRepository(),
	}
}

func (r *KiteRepository) Store(kite *model.Kite) (err error) {
	log.WithField("Record", kite).Debug("going to persist kite")

	if kite.BagID != uuid.Nil && !r.bagRepository.Exists(kite.BagID) {
		return fmt.Errorf("bag %s is not persisted yet", kite.BagID)
	}

	if kite.PictureID != uuid.Nil && !r.pictureRepository.Exists(kite.PictureID) {
		return fmt.Errorf("picture %s is not persisted yet", kite.PictureID)
	}

	_, err = db.Model(kite).Insert()

	if err == nil && kite.SpareParts != nil {
		for _, part := range kite.SpareParts {
			log.WithField("Record", part).Debug("going to add spare part to kite")

			if !r.sparePartRepository.Exists(part.ID) {
				return fmt.Errorf("spare part %s is not persisted yet", part.ID)
			}

			err := r.AddSparePart(kite, part)
			if err != nil {
				return err
			}
		}
	}

	return err
}

func (r *KiteRepository) Trash(kite *model.Kite) (err error) {
	log.WithField("ID", kite.ID).Debug("going to soft delete kite in storage")
	_, err = db.Model(kite).Delete()
	return err
}

func (r *KiteRepository) ForceDelete(kite *model.Kite) (err error) {
	log.WithField("ID", kite.ID).Debug("going to finally remove kite from storage")

	if kite.SpareParts != nil {
		for _, part := range kite.SpareParts {
			err := r.RemoveSparePart(kite, part)
			if err != nil {
				return err
			}
		}
	}

	_, err = db.Model(kite).ForceDelete()
	return err
}

func (r *KiteRepository) Recycle(id uuid.UUID) (err error) {
	kite := &model.Kite{ID: id}
	query := db.Model(kite).WherePK().Deleted().Limit(1)
	err = query.Select()

	if err == nil {
		log.WithField("ID", id).Debug("found soft deleted kite in storage")
		query := db.Model(kite).WherePK().Deleted().Set("deleted_at = ?", nil)
		_, err = query.Update()

		if err == nil {
			log.WithField("ID", kite.ID).Debug("recycled kite in storage")
		}
	}

	return err
}

func (r *KiteRepository) FetchByID(id uuid.UUID) (kite *model.Kite, err error) {
	log.WithField("ID", id).Debug("trying to find kite in storage")

	kite = &model.Kite{ID: id}
	query := db.Model(kite).Relation("Bag").Relation("Picture").Relation("SpareParts").WherePK()
	err = query.Select()

	// Handle Postgres 404 error
	if errors.As(err, &pg.ErrNoRows) {
		err = &NotFoundError{ID: id.String(), Err: err}
	}

	return kite, err
}

func (r *KiteRepository) AddSparePart(kite *model.Kite, part *model.SparePart) (err error) {
	kitePart := &model.KitePart{
		KiteID:      kite.ID,
		SparePartID: part.ID,
	}

	_, err = db.Model(kitePart).Insert()
	return err
}

func (r *KiteRepository) RemoveSparePart(kite *model.Kite, part *model.SparePart) (err error) {
	kitePart := &model.KitePart{
		KiteID:      kite.ID,
		SparePartID: part.ID,
	}

	_, err = db.Model(kitePart).Delete()
	return err
}
