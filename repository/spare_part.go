package repository

import (
	uuid "github.com/satori/go.uuid"

	"gitlab.com/talex-coding-dojo/kite-base/repository/model"
)

type SparePartRepository struct{}

func GetSparePartRepository() *SparePartRepository {
	return &SparePartRepository{}
}

func (r *SparePartRepository) Store(picture *model.SparePart) (err error) {
	_, err = db.Model(picture).Insert()
	return err
}

func (r *SparePartRepository) Delete(picture *model.SparePart) (err error) {
	_, err = db.Model(picture).Delete()
	return err
}

func (r *SparePartRepository) Exists(id uuid.UUID) (result bool) {
	count, _ := db.Model((*model.SparePart)(nil)).Where("uuid = ?", id).Count()
	return count == 1
}
