package middleware

import (
	"fmt"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
)

func getClientIP(c *gin.Context) string {
	requester := c.Request.Header.Get("X-Forwarded-For")

	if len(requester) == 0 {
		requester = c.Request.RemoteAddr
	}

	return requester
}

func getRequestHeader(c *gin.Context, key string) string {
	return c.Request.Header.Get(key)
}

func GetDurationInMillis(start time.Time) float64 {
	end := time.Now()
	duration := end.Sub(start)

	milliseconds := float64(duration) / float64(time.Millisecond)
	rounded := float64(int(milliseconds*100+.5)) / 100

	return rounded
}

func mapAsString(m map[string]string) string {
	kvp := make([]string, 0, len(m))
	for k := range m {
		kvp = append(kvp, fmt.Sprintf("%s: %q", k, m[k]))
	}
	return "{" + strings.Join(kvp, ", ") + "}"
}
