package middleware

import (
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"

	"gitlab.com/talex-coding-dojo/kite-base/config"
)

func JsonRequestLoggerMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Start timer
		start := time.Now()

		// Define some logging attributes
		fields := log.Fields{
			"RequestMethod": c.Request.Method,
			"RequestURI":    c.Request.RequestURI,
			"ClientIp":      getClientIP(c),
			"UserAgent":     getRequestHeader(c, "User-Agent"),
		}

		// Log at request start
		log.WithFields(fields).Info("received request")

		// Process next request
		c.Next()

		// Stop timer
		duration := GetDurationInMillis(start)

		// Redefine logging attributes
		fields = log.Fields{
			"RequestMethod": c.Request.Method,
			"RequestURI":    c.Request.RequestURI,
			"Latency":       fmt.Sprintf("%f", duration),
			"Status":        fmt.Sprintf("%d", c.Writer.Status()),
		}
		entry := log.WithFields(fields)

		// Log middleware error context
		if len(c.Errors) > 0 {
			for _, e := range c.Errors {
				entry.WithError(e)
			}
		}

		// Log at request end
		if c.Writer.Status() >= 500 {
			entry.Error("request failed")
		} else if c.Writer.Status() >= 400 {
			entry.Warn("request completed with warnings")
		} else {
			entry.Info("request completed")
		}
	}
}

func InitLogrus() {
	if "json" == strings.ToLower(config.App.Logging.Format) {
		log.SetFormatter(&log.JSONFormatter{})
	} else {
		log.SetFormatter(&log.TextFormatter{})
	}

	log.SetOutput(os.Stdout)

	level, _ := log.ParseLevel(config.App.Logging.Level)
	log.SetLevel(level)

	fields := log.Fields{"Format": config.App.Logging.Format, "Level": log.GetLevel()}
	log.WithFields(fields).Warn("configured Logrus logger")
}
