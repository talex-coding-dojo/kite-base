package middleware

import (
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
)

func RequestLoggerMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Start timer
		start := time.Now()

		// Define some logging attributes
		fields := map[string]string{
			"ClientIp":  getClientIP(c),
			"UserAgent": getRequestHeader(c, "User-Agent"),
		}

		// Log at request start
		fmt.Println("Received request", c.Request.Method, c.Request.RequestURI, mapAsString(fields))

		// Process next request
		c.Next()

		// Stop timer
		duration := GetDurationInMillis(start)

		// Redefine logging attributes
		fields = map[string]string{
			"Latency": fmt.Sprintf("%f", duration),
			"Status":  fmt.Sprintf("%d", c.Writer.Status()),
		}

		// Log at request end
		fmt.Println("Request completed", c.Request.Method, c.Request.RequestURI, mapAsString(fields))
	}
}
