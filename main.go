package main

import (
	"github.com/penglongli/gin-metrics/ginmetrics"
	_ "github.com/sakirsensoy/genv/dotenv/autoload"

	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"

	"gitlab.com/talex-coding-dojo/kite-base/config"
	"gitlab.com/talex-coding-dojo/kite-base/controller"
	"gitlab.com/talex-coding-dojo/kite-base/middleware"
	"gitlab.com/talex-coding-dojo/kite-base/nats"
	"gitlab.com/talex-coding-dojo/kite-base/repository"
)

func init() {
	// Init connection to NATS streaming server
	nats.InitNATS()
}

func main() {
	var (
		router            *gin.Engine
		bagController     = controller.GetBagController()
		pictureController = controller.GetPictureController()
		kiteController    = controller.GetKiteController()
	)

	// Init logger
	middleware.InitLogrus()

	// Start interrupt handler
	trapInterrupts()

	// Start timer
	start := time.Now()

	// Init database connection
	repository.InitDB()

	// Config router instance
	if config.App.ProductionMode {
		gin.SetMode(gin.ReleaseMode)
	}

	// Init new Gin router
	router = gin.New()
	metricsRouter := gin.Default()

	// Add middleware
	router.Use(gin.Recovery())
	router.Use(middleware.JsonRequestLoggerMiddleware())

	// Add routes for Bag entity
	bags := router.Group("/bags")
	{
		bags.GET("", bagController.Index)
		bags.POST("", bagController.Create)
		bags.GET(":id", bagController.Show)
		bags.PUT(":id", bagController.Update)
		bags.DELETE(":id", bagController.Destroy)
	}

	// Add routes for Picture entity
	pictures := router.Group("/pictures")
	{
		pictures.GET("", pictureController.Index)
		pictures.POST("", pictureController.Create)
		pictures.GET(":id", pictureController.Show)
		pictures.DELETE(":id", pictureController.Destroy)
		pictures.GET(":id/render", pictureController.Render)

	}

	// Add routes for Kite entity
	kites := router.Group("/kites")
	{
		kites.POST("", kiteController.Create)
		kites.GET(":id", kiteController.Show)
	}

	// Get global Monitor object
	m := ginmetrics.GetMonitor()
	m.SetMetricPath("/metrics")
	m.SetSlowTime(10)
	m.SetDuration([]float64{0.1, 0.3, 1.2, 5, 10})

	m.UseWithoutExposingEndpoint(router)
	m.Expose(metricsRouter)

	go func() {
		log.WithFields(log.Fields{"port": "2112"}).Info("starting Prometheus export: :2112/metrics")
		_ = metricsRouter.Run(":2112")
	}()

	// Setup some custom metrics for events
	counterSuccessMetric := &ginmetrics.Metric{
		Type:        ginmetrics.Counter,
		Name:        "emitted_events_success_count",
		Description: "Counter to track total amount of successfully emitted events",
		Labels:      []string{"subject"},
	}
	_ = ginmetrics.GetMonitor().AddMetric(counterSuccessMetric)

	counterFailureMetric := &ginmetrics.Metric{
		Type:        ginmetrics.Counter,
		Name:        "emitted_events_failure_count",
		Description: "Counter to track total amount of failed emitted events",
		Labels:      []string{"subject"},
	}
	_ = ginmetrics.GetMonitor().AddMetric(counterFailureMetric)

	// Stop timer
	duration := middleware.GetDurationInMillis(start)
	log.WithField("Latency", duration).Info("application startup completed")

	// Start server
	svrAddress := fmt.Sprintf("%s:%d", config.App.IpAddress, config.App.Port)
	fields := log.Fields{"ServerAddress": svrAddress, "ProductionMode": config.App.ProductionMode}
	log.WithFields(fields).Warn("running server")

	err := router.Run(svrAddress)
	if err != nil {
		log.WithFields(fields).Panic("stopped server")
	}
}

func trapInterrupts() {
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)

	go func() {
		sig := <-sigs

		repository.CloseDB()

		log.WithField("Signal", sig).Warn("exited server")
		os.Exit(1)
	}()
}
