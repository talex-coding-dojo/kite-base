# K8s Deployment

## Minikube

```shell
minikube start --kubernetes-version=v1.26.3
```

## Helm Charts

### NATS

```shell
helm repo add nats https://nats-io.github.io/k8s/helm/charts/
kubectl create ns nats
helm install nats nats/nats -f nats-values.yaml -n nats
```

```shell
kubectl port-forward --namespace nats svc/nats 4222 8222
```

### Postgres

```shell
kubectl create ns postgresql
helm install postgresql oci://registry-1.docker.io/bitnamicharts/postgresql -n postgresql
```

```shell
export POSTGRES_PASSWORD=$(kubectl get secret --namespace postgresql postgresql -o jsonpath="{.data.postgres-password}" | base64 -d)
kubectl run postgresql-client --rm --tty -i --restart='Never' --namespace postgresql --image docker.io/bitnami/postgresql:15.3.0-debian-11-r7 --env="PGPASSWORD=$POSTGRES_PASSWORD" --command -- psql --host postgresql -U postgres -d postgres -p 5432
```

```shell
kubectl port-forward --namespace postgresql svc/postgresql 5544:5432
```

```postgresql
create database kite_base;
create user kite with encrypted password 'secret';
grant all privileges on database kite_base to kite;
grant all on schema public to kite;
```

## Dockerize

```shell
docker login registry.gitlab.com
docker build -t registry.gitlab.com/talex-coding-dojo/kite-base:latest .
docker push registry.gitlab.com/talex-coding-dojo/kite-base:latest
```

## Application

```shell
kubectl apply -f k8s/manifests
kubectl delete -f k8s/manifests
```