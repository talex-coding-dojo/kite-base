package handler

import (
	"fmt"
	"github.com/nats-io/nats.go"
	"gitlab.com/talex-coding-dojo/kite-base/middleware"
	"time"

	"github.com/golang/protobuf/proto"
	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"

	"gitlab.com/talex-coding-dojo/kite-base/mapper"
	"gitlab.com/talex-coding-dojo/kite-base/pb"
	"gitlab.com/talex-coding-dojo/kite-base/repository"
	"gitlab.com/talex-coding-dojo/kite-base/service"
)

func PictureCreatedHandler(m *nats.Msg) {
	var (
		logEntry = log.WithField("Subject", m.Subject)

		envelope pb.Envelope
		err      error
	)

	defer func(start time.Time) {
		duration := middleware.GetDurationInMillis(start)
		log.WithFields(log.Fields{
			"Latency": duration,
			"Handler": "PictureCreated",
		}).Debugf("handler completed after %f ms", duration)
	}(time.Now())

	// TODO: happy path implementation, maybe add some failover handling

	err = proto.Unmarshal(m.Data, &envelope)
	if err != nil {
		logEntry.WithError(err).Errorf("could not deserialize %q event", m.Subject)
		return
	}

	logEntry = logEntry.WithField("CorrelationId", envelope.CorrelationId)

	switch payload := envelope.Payload.(type) {
	case *pb.Envelope_Event:
		switch event := payload.Event.Event.(type) {
		case *pb.Event_PictureCreated:
			pictureUUID := event.PictureCreated.Id.Value
			mediaFile := event.PictureCreated.GetPicture()

			logEntry = logEntry.WithField("PictureUUID", pictureUUID)

			dtoPicture, err := mapper.GetProtoMapper().FromPictureCreated(uuid.NewV4().String(), mediaFile)
			if err != nil {
				logEntry.WithError(err).Errorf("could not transform %q event", m.Subject)
				return
			}

			tnPicture, err := service.GetPictureServiceInstance().CreateThumbnail(dtoPicture)
			if err != nil {
				logEntry.WithError(err).Error("could not render thumbnail picture")
				return
			}

			dbPicture, err := mapper.GetPictureMapper().ToDB(tnPicture)
			if err != nil {
				logEntry.WithError(err).Error("could not transform DTO to DB model")
				return
			}

			err = repository.GetPictureRepository().Store(dbPicture)
			if err != nil {
				logEntry.WithError(err).Error("could not store thumbnail picture")
				return
			}

			// TODO: emit ThumbnailRendered event

			logEntry.WithField("ThumbnailUUID", dbPicture.ID).Info("processing successful, created new thumbnail")
		default:
			logEntry.WithField("Event", fmt.Sprintf("%v", event)).Error("event not supported")
		}
	default:
		logEntry.WithField("Envelope", fmt.Sprintf("%v", &envelope)).Error("payload not supported")
	}
}
