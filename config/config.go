package config

import "github.com/sakirsensoy/genv"

type natsConfig struct {
	ServerURL          string
	ClientID           string
	MaxRedeliveryCount uint32
	EnableDeadLetters  bool
}

type postgresConfig struct {
	Hostname string
	Port     int
	Username string
	Password string
	Database string
}

type databaseConfig struct {
	Postgres postgresConfig
}

type loggingConfig struct {
	Level  string
	Format string
}

type appConfig struct {
	NATS     natsConfig
	Database databaseConfig
	Logging  loggingConfig

	IpAddress      string
	Port           int
	ProductionMode bool
}

var App = &appConfig{
	NATS: natsConfig{
		ServerURL:          genv.Key("NATS_SERVER_URL").Default("nats://localhost:4222").String(),
		ClientID:           genv.Key("NATS_CLIENT_ID").Default("kite-base").String(),
		MaxRedeliveryCount: 5,
		EnableDeadLetters:  false,
	},
	Database: databaseConfig{
		Postgres: postgresConfig{
			Hostname: genv.Key("POSTGRES_HOSTNAME").Default("localhost").String(),
			Port:     genv.Key("POSTGRES_PORT").Default(5432).Int(),
			Username: genv.Key("POSTGRES_USERNAME").Default("kite").String(),
			Password: genv.Key("POSTGRES_PASSWORD").Default("secret").String(),
			Database: genv.Key("POSTGRES_DATABASE").Default("kite_base").String(),
		},
	},
	Logging: loggingConfig{
		Level:  genv.Key("LOGGING_LEVEL").Default("debug").String(),
		Format: genv.Key("LOGGING_FORMAT").Default("text").String(),
	},
	IpAddress:      genv.Key("APP_IP_ADDRESS").Default("0.0.0.0").String(),
	Port:           genv.Key("APP_PORT").Default(9090).Int(),
	ProductionMode: genv.Key("APP_PRODUCTION_MODE").Default(false).Bool(),
}
