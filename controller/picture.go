package controller

import (
	"net/http"

	"github.com/gin-gonic/gin"
	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"

	dto "gitlab.com/talex-coding-dojo/kite-base/controller/model"
	"gitlab.com/talex-coding-dojo/kite-base/repository"
	"gitlab.com/talex-coding-dojo/kite-base/service"
)

type PictureController struct {
	service *service.PictureService
}

func GetPictureController() *PictureController {
	return &PictureController{
		service: service.GetPictureServiceInstance(),
	}
}

func (c *PictureController) Create(ctx *gin.Context) {
	var (
		data    dto.PictureCreation
		picture *dto.Picture
		err     error
	)

	// Unmarshall JSON request body
	err = ctx.ShouldBindJSON(&data)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}

	// Delegate to service layer
	picture, err = c.service.Create(&data)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	// Render result
	ctx.JSON(http.StatusCreated, picture)
}

func (c *PictureController) Show(ctx *gin.Context) {
	// Access path parameter
	id := ctx.Param("id")

	// Convert to UUID
	uid, err := uuid.FromString(id)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}

	// Delegate to service layer
	picture, err := c.service.FindOneByUUID(uid)
	if err != nil {
		switch err.(type) {
		case *repository.NotFoundError:
			ctx.JSON(http.StatusNotFound, err.Error())
		default:
			ctx.JSON(http.StatusInternalServerError, err.Error())
		}
		return
	}

	// Render result
	ctx.JSON(http.StatusOK, picture)
}

func (c *PictureController) Render(ctx *gin.Context) {
	// Access path parameter
	id := ctx.Param("id")

	// Convert to UUID
	uid, err := uuid.FromString(id)
	if err != nil {
		ctx.AbortWithStatus(http.StatusBadRequest)
		return
	}

	// Delegate to service layer
	picture, mimeType, err := c.service.RenderImageByUUID(uid)
	if err != nil {
		log.WithError(err).Error("cannot render image")

		switch err.(type) {
		case *repository.NotFoundError:
			ctx.AbortWithStatus(http.StatusNotFound)
		default:
			ctx.AbortWithStatus(http.StatusInternalServerError)
		}
		return
	}

	// Render result
	ctx.Data(http.StatusOK, mimeType, picture)
}

func (c *PictureController) Index(ctx *gin.Context) {
	var (
		pictures []*dto.Picture
		err      error
	)

	pictures, err = c.service.FindAll()

	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	// Catch empty result list
	if pictures == nil {
		pictures = make([]*dto.Picture, 0)
	}

	// Render results
	ctx.JSON(http.StatusOK, pictures)
}

func (c *PictureController) Destroy(ctx *gin.Context) {
	// Access path parameter
	id := ctx.Param("id")

	// Convert to UUID
	uid, err := uuid.FromString(id)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}

	// Delegate to service layer
	err = c.service.Destroy(uid)
	if err != nil {
		switch err.(type) {
		case *repository.NotFoundError:
			ctx.JSON(http.StatusNotFound, err.Error())
		default:
			ctx.JSON(http.StatusInternalServerError, err.Error())
		}
		return
	}

	// Render result
	ctx.JSON(http.StatusNoContent, nil)
}
