package model

import (
	uuid "github.com/satori/go.uuid"
)

type BaseSparePart struct {
	Name        string `json:"name" binding:"required"`
	Description string `json:"summary" binding:"required"`
	Quantity    int    `json:"quantity" binding:"required"`
}

type SparePart struct {
	BaseSparePart

	UUID uuid.UUID `json:"uuid" format:"uuid"`
}
