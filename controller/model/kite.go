package model

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

type BaseKite struct {
	DisplayName      string `json:"display_name"`
	ManufacturerName string `json:"manufacturer_name"`
	ModelName        string `json:"model_name"`
	NumLines         int    `json:"num_lines"`
	Stackable        bool   `json:"stackable"`

	Wind string `json:"wind_level"`
	Type string `json:"construction_type"`

	AdditionalAttributes map[string]string `json:"attributes"`
}
type KiteCreation struct {
	BaseKite

	PictureID uuid.UUID `json:"picture_uuid"`
	BagID     uuid.UUID `json:"bag_uuid"`

	SpareParts []*BaseSparePart `json:"spare_parts"`
}

type Kite struct {
	BaseKite

	UUID uuid.UUID `json:"uuid" format:"uuid"`

	Picture    *Picture     `json:"picture"`
	Bag        *SimpleBag   `json:"bag"`
	SpareParts []*SparePart `json:"spare_parts"`

	CreatedAt  time.Time `json:"created_at"`
	ModifiedAt time.Time `json:"modified_at"`
}
