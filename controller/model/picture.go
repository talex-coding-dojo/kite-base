package model

import (
	"strings"

	uuid "github.com/satori/go.uuid"
)

type BasePicture struct {
	Description string `json:"description" binding:"required"`
	Height      int32  `json:"height" binding:"required"`
	Width       int32  `json:"width" binding:"required"`
	Data        string `json:"data" binding:"required"`
}

type PictureCreation struct {
	BasePicture
}

type Picture struct {
	BasePicture

	UUID uuid.UUID `json:"uuid" format:"uuid"`
}

func (p *BasePicture) MimeType() string {
	parts := strings.Split(p.Data, ";")
	mimeType := strings.Split(parts[0], ":")

	return mimeType[1]
}
