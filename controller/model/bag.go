package model

import (
	uuid "github.com/satori/go.uuid"
)

type BaseBag struct {
	Name string `json:"name" binding:"required"`
}
type BagCreation struct {
	BaseBag
}

type BagModify struct {
	BaseBag
}

type SimpleBag struct {
	BaseBag

	UUID uuid.UUID `json:"uuid" format:"uuid"`
}

type Bag struct {
	BaseBag

	UUID     uuid.UUID `json:"uuid" format:"uuid"`
	NumKites int       `json:"num_kites"`
}
