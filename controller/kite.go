package controller

import (
	"net/http"

	"github.com/gin-gonic/gin"
	uuid "github.com/satori/go.uuid"

	dto "gitlab.com/talex-coding-dojo/kite-base/controller/model"
	"gitlab.com/talex-coding-dojo/kite-base/repository"
	"gitlab.com/talex-coding-dojo/kite-base/service"
)

type KiteController struct {
	service *service.KiteService
}

func GetKiteController() *KiteController {
	return &KiteController{
		service: service.GetKiteServiceInstance(),
	}
}

func (c *KiteController) Create(ctx *gin.Context) {
	var (
		data dto.KiteCreation
		kite *dto.Kite
		err  error
	)

	// Unmarshall JSON request body
	err = ctx.ShouldBindJSON(&data)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}

	// Delegate to service layer
	kite, err = c.service.Create(&data)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	// Render result
	ctx.JSON(http.StatusCreated, kite)
}

func (c *KiteController) Show(ctx *gin.Context) {
	// Access path parameter
	id := ctx.Param("id")

	// Convert to UUID
	uid, err := uuid.FromString(id)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}

	// Delegate to service layer
	picture, err := c.service.FindOneByUUID(uid)
	if err != nil {
		switch err.(type) {
		case *repository.NotFoundError:
			ctx.JSON(http.StatusNotFound, err.Error())
		default:
			ctx.JSON(http.StatusInternalServerError, err.Error())
		}
		return
	}

	// Render result
	ctx.JSON(http.StatusOK, picture)
}
