package controller

import (
	"net/http"

	"github.com/gin-gonic/gin"
	uuid "github.com/satori/go.uuid"

	dto "gitlab.com/talex-coding-dojo/kite-base/controller/model"
	"gitlab.com/talex-coding-dojo/kite-base/repository"
	"gitlab.com/talex-coding-dojo/kite-base/service"
)

type BagController struct {
	service *service.BagService
}

func GetBagController() *BagController {
	return &BagController{
		service: service.GetBagServiceInstance(),
	}
}

func (c *BagController) Create(ctx *gin.Context) {
	var (
		data dto.BagCreation
		bag  *dto.Bag
		err  error
	)

	// Unmarshall JSON request body
	err = ctx.ShouldBindJSON(&data)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}

	// Delegate to service layer
	bag, err = c.service.Create(&data)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	// Render result
	ctx.JSON(http.StatusCreated, bag)
}

func (c *BagController) Show(ctx *gin.Context) {
	// Access path parameter
	id := ctx.Param("id")

	// Convert to UUID
	uid, err := uuid.FromString(id)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}

	// Delegate to service layer
	bag, err := c.service.FindOneByUUID(uid)
	if err != nil {
		switch err.(type) {
		case *repository.NotFoundError:
			ctx.JSON(http.StatusNotFound, err.Error())
		default:
			ctx.JSON(http.StatusInternalServerError, err.Error())
		}
		return
	}

	// Render result
	ctx.JSON(http.StatusOK, bag)
}

func (c *BagController) Index(ctx *gin.Context) {
	var (
		bags []*dto.Bag
		err  error
	)

	// Access query string parameter and delegate to service layer
	name := ctx.Request.URL.Query().Get("name")
	if name != "" {
		bags, err = c.service.FindAllByName(name)
	} else {
		bags, err = c.service.FindAll()
	}

	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	// Catch empty result list
	if bags == nil {
		bags = make([]*dto.Bag, 0)
	}

	// Render results
	ctx.JSON(http.StatusOK, bags)
}

func (c *BagController) Update(ctx *gin.Context) {
	var (
		data dto.BagModify
		bag  *dto.Bag
		err  error
	)

	// Access path parameter
	id := ctx.Param("id")

	// Convert to UUID
	uid, err := uuid.FromString(id)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}

	// Unmarshall JSON request body
	err = ctx.ShouldBindJSON(&data)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}

	// Delegate to service layer
	bag, err = c.service.Update(uid, &data)
	if err != nil {
		switch err.(type) {
		case *repository.NotFoundError:
			ctx.JSON(http.StatusNotFound, err.Error())
		default:
			ctx.JSON(http.StatusInternalServerError, err.Error())
		}
		return
	}

	// Render result
	ctx.JSON(http.StatusOK, bag)
}

func (c *BagController) Destroy(ctx *gin.Context) {
	// Access path parameter
	id := ctx.Param("id")

	// Convert to UUID
	uid, err := uuid.FromString(id)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}

	// Delegate to service layer
	err = c.service.Destroy(uid)
	if err != nil {
		switch err.(type) {
		case *repository.NotFoundError:
			ctx.JSON(http.StatusNotFound, err.Error())
		default:
			ctx.JSON(http.StatusInternalServerError, err.Error())
		}
		return
	}

	// Render result
	ctx.JSON(http.StatusNoContent, nil)
}
