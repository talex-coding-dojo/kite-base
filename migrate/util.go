package main

import (
	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"

	dbModel "gitlab.com/talex-coding-dojo/kite-base/repository/model"
)

func createSchema(db *pg.DB) (err error) {
	// tables will be created for these annotated models
	models := []interface{}{
		(*dbModel.Picture)(nil),
		(*dbModel.KitePart)(nil),
		(*dbModel.SparePart)(nil),
		(*dbModel.Kite)(nil),
		(*dbModel.Bag)(nil),
	}

	// load Postgres support for hstore columns
	_, err = db.ExecOne("CREATE EXTENSION IF NOT EXISTS hstore")
	if err != nil {
		return err
	}

	// iterate models and create missing tables
	for _, model := range models {
		err = db.Model(model).CreateTable(&orm.CreateTableOptions{
			Temp:        false,
			IfNotExists: true,
		})
		if err != nil {
			return err
		}
	}

	return nil
}
